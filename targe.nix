{ config, pkgs, ... }:

{
	imports = [
		./hardware-configuration.nix
	];

	boot.loader = {
		systemd-boot.enable = true;
		efi.canTouchEfiVariables = true;
	};

	networking = {
		hostName = "targe";
		useDHCP = false;
		interfaces.wlp2s0.useDHCP = true;
		networkmanager.enable = true;
		/* Open port for Synergy server. */
		firewall.extraCommands = "iptables -A nixos-fw -p tcp --source 192.168.1.0/24 --dport 24800 -j nixos-fw-accept";
	};
	services.openssh.enable = true;

	time.timeZone = "Europe/London";
	i18n.defaultLocale = "en_GB.UTF-8";
	console = {
		keyMap = "us";
	};

	services.xserver = {
		enable = true;
		videoDrivers = [ "nvidia" ];
		layout = "us,gb";
		xkbOptions = "grp:win_space_toggle";
		libinput.enable = true;
		desktopManager.mate.enable = true;
	};
	sound.enable = true;
	hardware.pulseaudio.enable = true;
	hardware.bluetooth.enable = true;
	services.blueman.enable = true;

	hardware.opengl.driSupport32Bit = true;
	hardware.nvidia.prime = {
		offload.enable = true;
		intelBusId = "PCI:0:2:0";
		nvidiaBusId = "PCI:1:0:0";
	};

	users.users.bladesy = {
		isNormalUser = true;
		extraGroups = [ "wheel" "networkmanager" "scanner" "lp" ];
	};

	virtualisation.docker = {
		enable = true;
		enableNvidia = true;
	};

	nixpkgs.config.packageOverrides = pkgs: {
		mypkgs = import (pkgs.fetchFromBitbucket {
				name = "mypkgs";
				owner = "Bladesy";
				repo = "nix-mypkgs";
				rev = "f43dda36523b";
				sha256 = "1cwfz9ild4h5a83ysi44s0rp7b3a6019rnjljzbg0b29rjjdbjb2"; })
			{ inherit pkgs; };
	};
	environment.defaultPackages = [];
	environment.systemPackages = with pkgs; [
		vim

		barrier
		networkmanagerapplet
		blueman
		qt5ct

		firefox
		bitwarden
		tutanota-desktop
		tdesktop

		jetbrains.clion

		mypkgs.ubuntu-mate-themes
		mypkgs.prospect-mail
	];
	environment.sessionVariables.EDITOR = "vim";
	environment.sessionVariables.QT_QPA_PLATFORMTHEME = "qt5ct";

	services.xserver.displayManager.lightdm.background
		= "${pkgs.mate.mate-backgrounds}/share/backgrounds/mate/nature/GreenMeadow.jpg";
	services.xserver.displayManager.lightdm.greeters.gtk.theme.package
		= pkgs.mypkgs.ubuntu-mate-themes;
	services.xserver.displayManager.lightdm.greeters.gtk.theme.name
		= "Yaru-MATE-dark";
	services.xserver.displayManager.lightdm.greeters.gtk.iconTheme.package
		= pkgs.mypkgs.ubuntu-mate-themes;
	services.xserver.displayManager.lightdm.greeters.gtk.iconTheme.name
		= "Ambiant-MATE";
	services.xserver.displayManager.lightdm.greeters.gtk.cursorTheme.package
		= pkgs.mate.mate-themes;
	services.xserver.displayManager.lightdm.greeters.gtk.cursorTheme.name
		= "mate-black";

	services.printing.enable = true;
	services.printing.drivers = [ pkgs.hplipWithPlugin ];
	hardware.sane.enable = true;
	hardware.sane.extraBackends = [ pkgs.hplipWithPlugin ];

	nixpkgs.config.allowUnfree = true;
	system.stateVersion = "21.11";
}
