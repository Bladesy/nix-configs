{ config, pkgs, ... }:

{
	imports = [
		./hardware-configuration.nix
	];

	boot.loader = {
		systemd-boot.enable = true;
		efi.canTouchEfiVariables = true;
	};

	networking = {
		hostName = "manuscript";
		useDHCP = false;
		interfaces.wlo1.useDHCP = true;
		networkmanager.enable = true;
		firewall.extraCommands = "iptables -A nixos-fw -p tcp --source 192.168.1.0/24 --dport 24800 -j nixos-fw-accept";
	};
	services.openssh.enable = true;

	time.timeZone = "Europe/London";
	i18n.defaultLocale = "en_GB.UTF-8";
	console = {
		keyMap = "uk";
	};

	services.xserver = {
		enable = true;
		layout = "gb";
		libinput.enable = true;
		desktopManager.mate.enable = true;
	};
	sound.enable = true;
	hardware.pulseaudio.enable = true;
	hardware.bluetooth.enable = true;
	services.blueman.enable = true;

	users.users.bladesy = {
		isNormalUser = true;
		extraGroups = [ "wheel" "networkmanager" ];
	};

	virtualisation.docker.enable = true;
	
	nixpkgs.config.packageOverrides = pkgs: {
		mypkgs = import (pkgs.fetchFromBitbucket {
				name = "mypkgs";
				owner = "Bladesy";
				repo = "nix-mypkgs";
				rev = "f43dda36523b";
				sha256 = "1cwfz9ild4h5a83ysi44s0rp7b3a6019rnjljzbg0b29rjjdbjb2"; })
			{ inherit pkgs; };
	};
	environment.defaultPackages = [];
	environment.systemPackages = with pkgs; [
		vim

		barrier
		networkmanagerapplet
		blueman

		firefox
		bitwarden
		tutanota-desktop
		tdesktop

		jetbrains.clion

		mypkgs.ubuntu-mate-themes
		mypkgs.prospect-mail
	];
	environment.sessionVariables.EDITOR = "vim";

	services.xserver.displayManager.lightdm.background
		= "${pkgs.mate.mate-backgrounds}/share/backgrounds/mate/nature/GreenMeadow.jpg";
	services.xserver.displayManager.lightdm.greeters.gtk.theme.package
		= pkgs.mypkgs.ubuntu-mate-themes;
	services.xserver.displayManager.lightdm.greeters.gtk.theme.name
		= "Yaru-MATE-dark";
	services.xserver.displayManager.lightdm.greeters.gtk.iconTheme.package
		= pkgs.mypkgs.ubuntu-mate-themes;
	services.xserver.displayManager.lightdm.greeters.gtk.iconTheme.name
		= "Ambiant-MATE";
	services.xserver.displayManager.lightdm.greeters.gtk.cursorTheme.package
		= pkgs.mate.mate-themes;
	services.xserver.displayManager.lightdm.greeters.gtk.cursorTheme.name
		= "mate-black";

	nixpkgs.config.allowUnfree = true;
	system.stateVersion = "21.11";
}
